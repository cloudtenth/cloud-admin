import '@babel/polyfill';
import Vue from 'vue';
import App from './App.vue';
import admin from './admin';
import cloud from 'cloud-vue';
import './App.css';
import VueJsonp from 'vue-jsonp'
Vue.use(VueJsonp)
Vue.use(admin);

// Vue.prototype.$cloud = cloud.connect({
//   AppURL: 'http://localhost:8080',
//   AppName: 'ddd'// 识别名称
// });

let URL = process.env.NODE_ENV === 'production' ? 'https://main.manong.cloud' : 'http://localhost:8080';

// console.log('当前提交URL', URL);

Vue.prototype.$cloud = cloud.connect({
  AppURL: URL,
  AppName: 'admin', // 识别名称
  AppID: 'mnc08c25f8136d590c',
  AppSecret: '3AYpU16dZ1CY7ejqvrE39B351vanLJVD'
});

export default new Vue({
  router: admin.router(),
  store: admin.store(),
  render: h => h(App)
}).$mount('#app');
