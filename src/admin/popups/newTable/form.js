/*
 * @Author: sean
 * @Date:   2020-06-02 19:56:57
 * @Last Modified by:   sean
 * @Last Modified time: 2020-06-20 19:13:50
 */
export default {
  'name': {
    'name': 'name',
    'title': '表格名称',
    'type': 'String',
    'required': true,
    form: {
      type: 'input',
      rules: {
        valid: {
          pattern: /^[a-zA-Z]\w+$/,
          message: '菜单名称必须为字母开头'
        }
      }
    }
  },
  'explain': {
    'name': 'explain',
    'title': '表格说明',
    'type': 'String',
    form: {
      type: 'input'
    }
  },
  'auth': {
    'name': 'auth',
    'title': '权限',
    'type': 'Number',
    'default': 1,
    form: {
      type: 'radio',
      value: [
        { title: '所有用户可读，仅创建者可读写', key: 1 },
        { title: '仅创建者可读写', key: 2 },
        { title: '所有用户可读', key: 3 },
        { title: '所有用户不可读写', key: 4 }
      ]
    }
  }
};
