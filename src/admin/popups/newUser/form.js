/*
 * @Author: sean
 * @Date:   2020-06-02 19:56:57
 * @Last Modified by:   Qinuoyun
 * @Last Modified time: 2020-07-01 16:27:04
 */
export default {
  'username': {
    'name': 'username',
    'title': '用户名',
    'type': 'String',
    'required': true,
    form: {
      type: 'input',
      rules: {
        valid: {
          pattern: /^[a-zA-Z]\w+$/,
          message: '菜单名称必须为字母开头'
        }
      }
    }
  },
  'email': {
    'name': 'email',
    'title': '邮箱',
    'type': 'String',
    form: {
      type: 'input'
    }
  },
  'mobile': {
    'name': 'mobile',
    'title': '手机号',
    'type': 'String',
    'required': true,
    form: {
      type: 'input'
    }
  },
  'password': {
    'name': 'password',
    'title': '密码',
    'type': 'String',
    'required': true,
    form: {
      type: 'input',
      rules: {
        valid: {
          pattern: /^[a-zA-Z]\w+$/,
          message: '密码必须为字母开头'
        }
      }
    }
  },
  'role': {
    'name': 'role',
    'title': '角色',
    'type': 'String',
    'required': true,
    'form': {
      'type': 'select',
      'hide': false,
      'rules': {

      },
      'from': {
        'switch': true,
        'method': 'getRoleList',
        'field': {
          'parentName': '',
          'titleName': 'title',
          'keyName': 'name'
        },
        'value': {
          'parentName': '',
          'titleName': '',
          'keyName': ''
        }
      }
    }
  },
  '_uniqueid': {
    'name': '_uniqueid',
    'title': '对应商户',
    'type': 'String',
    'required': true,
    'form': {
      'type': 'select',
      'hide': false,
      'rules': {

      },
      'from': {
        'switch': true,
        'method': 'getUniqueid',
        'field': {
          'parentName': '',
          'titleName': 'title',
          'keyName': 'uuid'
        },
        'value': {
          'parentName': '',
          'titleName': '',
          'keyName': ''
        }
      }
    }
  }
};
