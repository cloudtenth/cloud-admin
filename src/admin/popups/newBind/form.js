/*
 * @Author: sean
 * @Date:   2020-06-02 19:56:57
 * @Last Modified by:   sean
 * @Last Modified time: 2020-07-09 21:49:20
 */

export default {
  'AppID': {
    'name': 'AppID',
    'title': 'AppID',
    'type': 'String',
    'required': true,
    form: {
      type: 'input',
      rules: {
        valid: {
          pattern: /^[a-zA-Z]\w+$/,
          message: '菜单名称必须为字母开头'
        }
      }
    }
  },
  'AppSecret': {
    'name': 'AppSecret',
    'title': 'AppSecret',
    'type': 'String',
    'required': true,
    form: {
      type: 'input'
    }
  },
  'app_pay': {
    'name': 'app_pay',
    'title': '支付类型',
    'type': 'String',
    form: {
      type: 'input'
    }
  },
  'store_id': {
    'name': 'store_id',
    'title': '门店号',
    'type': 'String',
    form: {
      type: 'input'
    }
  },
  'mch_id': {
    'name': 'mch_id',
    'title': 'mch_id',
    'type': 'String',
    form: {
      type: 'input'
    }
  },
  'partnerKey': {
    'name': 'partnerKey',
    'title': 'partnerKey',
    'type': 'String',
    form: {
      type: 'input'
    }
  },
  'URL': {
    'name': 'URL',
    'title': 'URL',
    'type': 'String',
    'required': true,
    form: {
      type: 'input'
    }
  },
  'Token': {
    'name': 'Token',
    'title': 'Token',
    'type': 'String',
    'required': true,
    form: {
      type: 'input',
      rules: {
        valid: {
          pattern: /^[a-zA-Z]\w+$/,
          message: '菜单名称必须为字母开头'
        }
      }
    }
  },
  'EncodingAESKey': {
    'name': 'EncodingAESKey',
    'title': 'EncodingAESKey',
    'type': 'String',
    'required': true,
    form: {
      type: 'input',
      rules: {
        valid: {
          pattern: /^[a-zA-Z]\w+$/,
          message: '菜单名称必须为字母开头'
        }
      }
    }
  },
  'model': {
    'name': 'model',
    'title': '应用类型',
    'type': 'String',
    'default': 'vue',
    form: {
      hide: true,
      type: 'input'
    }
  }
};
