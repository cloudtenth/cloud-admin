/*
 * @Author: sean
 * @Date:   2020-06-02 19:56:57
 * @Last Modified by:   sean
 * @Last Modified time: 2020-06-20 19:14:44
 */
export default {
  'name': {
    'name': 'name',
    'title': '应用名称',
    'type': 'String',
    'required': true,
    form: {
      type: 'input',
      rules: {
        valid: {
          pattern: /^[a-zA-Z]\w+$/,
          message: '菜单名称必须为字母开头'
        }
      }
    }
  },
  'title': {
    'name': 'title',
    'title': '应用标题',
    'type': 'String',
    'required': true,
    form: {
      type: 'input'
    }
  },
  'type': {
    'name': 'type',
    'title': '应用类型',
    'type': 'String',
    'default': 'vue',
    form: {
      type: 'radio',
      value: [
        { title: '电脑端应用', key: 'vue' },
        { title: '移动端应用', key: 'uni' }
      ]
    }
  },
  'model': {
    'name': 'model',
    'title': '应用属性',
    'type': 'String',
    'default': 'weapp',
    form: {
      type: 'radio',
      value: [
        { title: '微信小程序', key: 'weapp' },
        { title: '支付宝小程序', key: 'alipay' },
        { title: '微信公众号', key: 'wechat' },
        { title: '安卓APP', key: 'android' },
        { title: '苹果APP', key: 'apple' },
        { title: '混合APP', key: 'app' },
        { title: '管理后台', key: 'admin' },
        { title: '博客应用', key: 'blog' },
        { title: '手机网站', key: 'web' }
      ]
    }
  }
};
