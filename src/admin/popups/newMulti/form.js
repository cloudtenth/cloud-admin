/*
 * @Author: sean
 * @Date:   2020-06-02 19:56:57
 * @Last Modified by:   sean
 * @Last Modified time: 2020-06-20 19:14:02
 */
export default {
  'name': {
    'name': 'name',
    'title': '商户名称',
    'type': 'String',
    'required': true,
    form: {
      type: 'input',
      rules: {
        valid: {
          pattern: /^[a-zA-Z]\w+$/,
          message: '菜单名称必须为字母开头'
        }
      }
    }
  },
  'title': {
    'name': 'title',
    'title': '商户标题',
    'type': 'String',
    'required': true,
    form: {
      type: 'input'
    }
  }
};
