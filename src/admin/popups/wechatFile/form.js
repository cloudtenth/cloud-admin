/*
 * @Author: sean
 * @Date:   2020-06-02 19:56:57
 * @Last Modified by:   Qinuoyun
 * @Last Modified time: 2020-06-22 16:12:03
 */
export default {
  'fileName': {
    'name': 'fileName',
    'title': '文件名(带后缀)',
    'type': 'String',
    'required': true,
    form: {
      type: 'input'
    }
  },
  'fileBody': {
    'name': 'fileBody',
    'title': '文件内容',
    'type': 'String',
    'required': true,
    form: {
      type: 'input'
    }
  }
};
