/*
 * @Author: sean
 * @Date:   2019-12-26 12:04:29
 * @Last Modified by:   Qinuoyun
 * @Last Modified time: 2020-03-17 17:06:51
 */
const old_modulesFiles = require.context('./', true, /index\.vue$/);

const old_modules = old_modulesFiles.keys().reduce((old_modules, old_moduleDir) => {
  const old_modulePath = old_moduleDir.replace(/^\.\/(.*)\.\w+$/, '$1');

  let old_moduleArr = old_modulePath.split('/');

  if (old_moduleArr.length === 2) {
    let group = old_moduleArr[0];
    old_modules[group] = old_modulesFiles(old_moduleDir);
  }
  return old_modules;
}, {});

const new_modulesFiles = require.context('@/popups', true, /index\.vue$/);

const new_modules = new_modulesFiles.keys().reduce((new_modules, new_moduleDir) => {
  const new_modulePath = new_moduleDir.replace(/^\.\/(.*)\.\w+$/, '$1');

  let new_moduleArr = new_modulePath.split('/');

  if (new_moduleArr.length === 2) {
    let group = new_moduleArr[0];
    new_modules[group] = new_modulesFiles(new_moduleDir);
  }
  return new_modules;
}, {});

export default Object.assign(old_modules, new_modules);
