/*
 * @Author: imac
 * @Date:   2020-05-30 15:02:36
 * @Last Modified by:   Qinuoyun
 * @Last Modified time: 2020-07-02 13:09:41
 */
import { install } from './install';
import util from './util';
import routeMap from './router';
import storeMap from './store';
import pages from '@/pages.json';
import config from './config.json';

export default class admin extends util {
  /**
   * 初始化安装器
   * @return {[type]} [description]
   */
  static install() {

  }

  /**
   * 处理路由
   * @param  {[type]} route [description]
   * @return {[type]}       [description]
   */
  static router(route = []) {
    let maps = [];
    if (this.is_array(route)) {
      maps = maps.concat(route);
    }
    // 合并配置文件
    this.config = this.extend(config, pages);
    // 执行路由函数
    return routeMap.apply(this, [maps, this.config]);
  }

  /**
   * 错误处理
   * @return {[type]} [description]
   */
  static error(data) {
    let msg = '';
    let code = '';
    if (data) {

    }
    console.log(`ErrorMessage::${data.msg} [${data.code}]`, `ERR - ${data.code}`);
    this.$Message({
      type: 'error',
      text: data.msg
    });
  }
  /**
   * 处理路由
   * @param  {[type]} route [description]
   * @return {[type]}       [description]
   */
  static store() {
    return storeMap();
  }
}

admin.install = install;
admin.version = '__VERSION__';

if (util.in_browser && window.Vue) {
  window.Vue.use(admin);
}
