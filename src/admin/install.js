import HeyUI from 'heyui';
import popups from './popups/index.js';
require('./style/app.less');
require('./style/admin.less');

export let _Vue;

export function install(Vue) {
  if (install.installed && _Vue === Vue) {
    return;
  }
  install.installed = true;

  _Vue = Vue;
  // 安装heyUI
  Vue.use(HeyUI);

  Vue.prototype.$admin = this;
  Vue.prototype.CloudAttribute = (resolve) => require(['./layout/attribute'], resolve);

  Vue.component('CloudForm', (resolve) => require(['./layout/form'], resolve));
  Vue.component('CloudAttribute', (resolve) => require(['./layout/attribute'], resolve));
  Vue.component('Chart', (resolve) => require(['./layout/chart'], resolve));

  Object.keys(popups).forEach(function (key, index) {
    let item = popups[key];
    Vue.directive(key, item);
  });
}
