import Vue from 'vue';
import Vuex from 'vuex';

import apply from './apply';

import old_getters from '@/store/getters.js';
import new_getters from './getters';

Vue.use(Vuex);

// https://webpack.js.org/guides/dependency-management/#requirecontext
const modulesFiles = require.context('@/store', true, /\.js$/);

// you do not need `import app from './app'`
// it will auto require all vuex module from modules file
const modules = modulesFiles.keys().reduce((modules, modulePath) => {
  const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1');
  if (moduleName !== 'getters') {
    const value = modulesFiles(modulePath);
    modules[moduleName] = value.default;
  }
  return modules;
}, {});

const initStore = (StoreMap) => {
  // 执行Vuex
  let Store = new Vuex.Store({
    modules: Object.assign({ apply }, (modules || {})),
    state: {

    },
    mutations: {

    },
    actions: {

    },
    getters: Object.assign(old_getters, new_getters)
  });
  return Store;
};

export default initStore;
