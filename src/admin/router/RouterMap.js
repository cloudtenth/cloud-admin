export default [{
  title: '后台管理',
  path: '',
  name: 'admin',
  key: 'admin',
  show: true,
  icon: '',
  children: [{
    title: '仪表盘',
    path: '/index/index',
    name: 'dashboard',
    key: 'dashboard',
    show: true,
    icon: 'icon-overview'
  },
  {
    title: '项目管理',
    path: '-',
    name: 'project',
    key: 'project',
    show: true,
    icon: 'icon-product',
    children: [{
      title: '我的项目',
      path: '/project/manage',
      name: 'manage',
      key: 'manage',
      show: true,
      icon: ''
    },
    {
      title: '我的应用',
      path: '/project/apply',
      name: 'apply',
      key: 'apply',
      show: true,
      icon: ''
    },
    {
      title: '表格管理',
      path: '/project/tables',
      name: 'tables',
      key: 'tables',
      show: true,
      icon: ''
    },
    {
      title: '数据表格',
      path: '/project/datas',
      name: 'datas',
      key: 'datas',
      show: true,
      icon: ''
    }, {
      title: '扩展绑定',
      path: '/project/binding',
      name: 'binding',
      key: 'binding',
      show: true,
      icon: ''
    }
    ]
  },
  {
    title: '系统管理',
    path: '-',
    name: 'system',
    key: 'system',
    show: true,
    icon: 'icon-setting',
    children: [{
      title: '菜单管理',
      path: '/system/menus',
      name: 'menus',
      key: 'menus',
      show: true,
      icon: ''
    },
    {
      title: '角色管理',
      path: '/system/roles',
      name: 'roles',
      key: 'roles',
      show: true,
      icon: ''
    },
    {
      title: '规则管理',
      path: '/system/rules',
      name: 'rules',
      key: 'rules',
      show: true,
      icon: ''
    },
    {
      title: '用户管理',
      path: '/system/users',
      name: 'users',
      key: 'users',
      show: true,
      icon: ''
    }
    ]
  }
  ]
}];
