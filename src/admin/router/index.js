/*
 * @Author: imac
 * @Date:   2020-05-30 16:45:24
 * @Last Modified by:   qinuoyun
 * @Last Modified time: 2020-07-31 14:24:57
 */
import Vue from 'vue';
import MD5 from 'js-md5';
import HeyUI from 'heyui';
import Router from 'vue-router';
import util from '../util';
import store from '../store';

let default_map = [{
  title: '页面找不到',
  path: '/error/404',
  name: 'NotfoundError',
  show: true,
  icon: ''
},
{
  title: '系统错误',
  path: '/error/500',
  name: 'SystemError',
  show: true,
  icon: ''
},
{
  title: '权限错误',
  path: '/error/403',
  name: 'PermissionError',
  show: true,
  icon: ''
},
{
  title: '页面找不到',
  path: '*',
  name: 'CommonNotfoundError',
  show: true,
  icon: ''
}, {
  title: 'login',
  path: '/login/index',
  key: 'login',
  icon: 'icon-monitor'
}, {
  title: 'cloud_apply_for',
  path: '/cloud_apply_for',
  key: 'cloud_apply_for',
  icon: 'icon-monitor'
}
];

// 设置默认路由
let default_route = {};
default_route[MD5('')] = (resolve) => require(['../layout/frame'], resolve);
default_route[MD5('-')] = (resolve) => require(['../layout/empty'], resolve);
default_route[MD5('/login/index')] = (resolve) => require(['../layout/login'], resolve);
default_route[MD5('/error/403')] = (resolve) => require(['../error/403'], resolve);
default_route[MD5('/error/404')] = (resolve) => require(['../error/404'], resolve);
default_route[MD5('/error/500')] = (resolve) => require(['../error/500'], resolve);
default_route[MD5('/cloud_apply_for')] = (resolve) => require(['../layout/apply'], resolve);
default_route[MD5('*')] = (resolve) => require(['../error/404'], resolve);

// 加载路由
Vue.use(Router);

// 通过WebPack context 加载文件
const componentsFiles = require.context('@/pages', true, /\.vue$/);
componentsFiles.keys().reduce((components, componentPath) => {
  // 正则匹配路径
  const componentName = componentPath.replace(/^\.\/(.*)\.\w+$/, '$1');
  // 读取文件内容
  const value = componentsFiles(componentPath);
  // 必须MD5加密 否则无法找到
  default_route[MD5('/' + componentName)] = value.default;
}, {});

/**
 * 格式化树形结构数据 生成 vue-router 层级路由表
 *
 * @param routerMap
 * @param parent
 * @returns {*}
 */
const generator = (routerMap, parent) => {
  return routerMap.map(item => {
    // console.log("读取", `@/views${item.path}`)
    const currentRouter = {
      // 如果路由设置了 path，则作为默认 path，否则 路由地址 动态拼接生成如 /dashboard/workplace
      path: item.path,
      // 路由名称，建议唯一
      name: item.name || '',
      // 该路由对应页面的 组件 :方案2 (动态加载)
      component: default_route[MD5(item.path)],
      // (() => import(`#/views${item.path}`))
      // components[item.path]
      // meta: 页面标题, 菜单图标, 页面权限(供指令权限用，可去掉)
      meta: {
        title: item.title,
        icon: item.icon || undefined
      }
    };

    // 判断父级存在并且父级为admin
    if (parent && parent.name == 'admin') {
      currentRouter.component = default_route[MD5('-')];
      // 如果子集不存在，需要重改Path
      if (!item.children) {
        let path = '/' + item.name + '/index';
        currentRouter.component = default_route[MD5(path)];
      }
    }

    // 判断是否有子集
    if (item.children) {
      currentRouter.redirect = item.children[0].path;
    }

    // 是否设置了隐藏菜单
    if (!item.show) {
      currentRouter.hidden = true;
    }

    // 为了防止出现后端返回结果不规范，处理有可能出现拼接出两个 反斜杠
    if (!currentRouter.path.startsWith('http')) {
      currentRouter.path = currentRouter.path.replace('//', '/');
    }
    // 重定向
    item.redirect && (currentRouter.redirect = item.redirect);
    // 是否有子菜单，并递归处理
    if (item.children && item.children.length > 0) {
      // Recursion
      currentRouter.children = generator(item.children, currentRouter);
    }
    return currentRouter;
  });
};

const initRouter = (RouterMap, config) => {
  let Maps = generator(default_map);
  // 执行路由
  let router = new Router({
    mode: 'history',
    scrollBehavior: () => ({ y: 0 }),
    routes: Maps
  });
  // 路由前置
  router.beforeEach((to, from, next) => {
    // 开启进度条
    HeyUI.$LoadingBar.start();
    // 开启$Cloud
    const $cloud = router.app.$cloud;
    // 开启$store
    const $store = store();

    // 如果APPID为空的时候
    if (util.is_null($cloud.AppID) && util.is_null($cloud.AppSecret)) {
      if (to.path !== '/cloud_apply_for') {
        next({ path: '/cloud_apply_for' });
      } else {
        next();
      }
    } else {
      // 如果APPID不为空的时候
      if (to.path == '/cloud_apply_for') {
        next({ path: '/' });
      }
      // 获取用户信息
      if (!util.is_null($cloud.userInfo())) {
        if (to.path === '/login/index') {
          next({ path: '/' });
        } else {
          if ($store.getters.menus.length === 0) {
            $store.dispatch('apply/GetInfo').then(data => {
              // 处理路由递归
              let Routers = generator(data);
              // 动态添加路由
              router.addRoutes(Routers);
              // 获取跳转参数
              const redirect = decodeURIComponent(from.query.redirect || to.path);
              if (to.path === redirect) {
                // hack方法 确保addRoutes已完成 ,设置replace: true，这样导航就不会留下历史记录
                next({ path: to.path, replace: true });
              } else {
                // 跳转到目的路由
                next({ path: redirect });
              }
            }).catch((error) => {
              log('error', error);
            });
          } else {
            next();
          }
        }
      } else {
        // 处理白名单配置
        if (config.whiteList.includes(to.name)) {
          // 在免登录白名单，直接进入
          next();
        } else {
          let toPath = '/';
          if (to.path !== '/login/index') {
            toPath = to.fullPath;
            next({ path: '/login/index', query: { redirect: toPath } });
          } else {
            next();
          }
        }
      }
    }
  });
  // 路由后置
  router.afterEach(() => {
    HeyUI.$LoadingBar.success();
  });
  return router;
};

export default initRouter;
