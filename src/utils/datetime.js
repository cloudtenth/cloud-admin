/*
* @Author: Silence24
* @Date:   2020-05-21 15:48:59
* @Last Modified by:   wiki
* @Last Modified time: 2020-08-19 14:47:47
*/




class datetime {

	constructor() {
        this.currentDate = null;    // 当前日期
		this.currentTime = null;    // 当前时间
		this.datetime = null ;
	}

	handle(date = new Date()){
		// 日期
	    let year = date.getFullYear();
	    let month = date.getMonth()+1 < 10  ? '0' + (date.getMonth()+1) : date.getMonth()+1
	    let days = date .getDate() < 10 ? '0' + date .getDate() : date .getDate();

	    // 时间
	    let hours = date.getHours() < 10 ? '0' + date.getHours() : date.getHours() ;
		let minutes = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
		let seconds = date.getSeconds() <10 ? '0' + date.getSeconds() : date.getSeconds();

		this.currentDate = year + '-' + month + '-' + days;
		this.currentTime = hours + ':' + minutes + ':' + seconds;
		this.datetime = year + '-' + month + '-' + days + ' ' + hours + ':' + minutes + ':' + seconds;
	}

	getDate(){
		this.handle();
		return this.currentDate;
	}

	getTime(){
		this.handle();
		return this.currentTime;
	}

	getDateTime(){
		this.handle();
		return this.datetime
	}


	/**
	 * 以某一天为点，获取未来的某一天
	 */
	getAfter(now,day,type = 'date'){
		let time = new Date(now)
		time = +time + day*1000*60*60*24;
		time = new Date(time);

		this.handle(time);
		return type === 'date'?this.currentDate:this.datetime
	}

	/**
	 * 以某一天为点，获取历史中的某一天
	 */
	getBefore(now,day,type = 'date'){
		let time = new Date(now)
		time = +time - day*1000*60*60*24;
		time = new Date(time);

		this.handle(time);
		return type === 'date'?this.currentDate:this.datetime
	}

}


export default new datetime;


