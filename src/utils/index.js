export const setCard = (value) => {
  let base_info = {
    'logo_url': value.logo_url[0] || '',
    'brand_name': value.brand_name,
    'code_type': 'CODE_TYPE_QRCODE',
    'title': value.title,
    'color': 'Color010',
    'notice': value.center_sub_title,
    'service_phone': value.service_phone || '',
    'description': value.description,
    'sku': {
      'quantity': Number(value.quantity)
    },
    'use_limit': Number(value.get_limit),
    'get_limit': Number(value.get_limit), // 用户获取上限
    'use_custom_code': false,
    'bind_openid': false,
    'can_share': false, // 卡券领取页面是否可分享。
    'can_give_friend': false, // 卡券是否可转赠。
    'location_id_list': value.location_id_list,
    'use_all_locations': false,
    'center_title': "进入首页",
    'center_sub_title': value.center_sub_title,
    'center_app_brand_user_name': 'gh_edd7d9006f19@app',
    'center_app_brand_pass': value.type == 'GIFT' ? '/pages/index/index' : '/pages/pay/index'
  };
  if (value.timeType === 1) {
    // 使用日期,有效期
    base_info.date_info = {
      'type': 'DATE_TYPE_FIX_TIME_RANGE', // 表示固定日期区间
      'begin_timestamp': new Date(value.time_quantum.start).getTime() / 1000, // 开始时间
      'end_timestamp': new Date(value.time_quantum.end).getTime() / 1000 // 结束时间
    };
  } else {
    base_info.date_info = {
      'type': 'DATE_TYPE_FIX_TERM', // 表示固定时长
      'fixed_term': Number(value.fixed_term), // 开始时间
      'fixed_begin_term': Number(value.fixed_begin_term) // 结束时间
    };
  }

  let advanced_info = {
    'abstract': {
      'abstract': value.abstract, // 封面摘要简介
      'icon_url_list': value.icon_url_list // 封面图片列表
    }
  };

  if (value.timeLimit === 2) {
    // 使用四段限制
    let time_limit = [];
    let begin_hour = 0;
    let end_hour = 0;
    let begin_minute = 23;
    let end_minute = 59;
    let time_array = [];
    //开始-输出时：分
    if (value.begin_time) {
      time_array = value.begin_time.split(':');
      begin_hour = time_array[0];
      begin_minute = time_array[1];

    }

    //结束-输出时：分
    if (value.end_time) {
      time_array = value.end_time.split(':');
      end_hour = time_array[0];
      end_minute = time_array[1];
    }

    //处理星期数据
    for (var i in value.time_type) {
      let obj = {
        'type': value.time_type[i], // 限制类型,星期
        'begin_hour': begin_hour, // 起始小时
        'end_hour': end_hour, // 起始分钟
        'begin_minute': begin_minute, // 截止小时
        'end_minute': end_minute // 截止分钟
      };
      time_limit.push(obj);
    }
    advanced_info.time_limit = time_limit;
  }

  // 处理数据
  let data = {

  };
  switch (value.type) {
    // 兑换券
    case 'GIFT':
      data = {
        card: {
          'card_type': 'GIFT',
          'gift': {
            'base_info': base_info,
            'advanced_info': advanced_info,
            'gift': value.gift
          }
        }
      };
      break;
      // 折扣券
    case 'DISCOUNT':
      data = {
        card: {
          'card_type': 'DISCOUNT',
          'discount': {
            'base_info': base_info,
            'advanced_info': advanced_info,
            'discount': 100 - (Number(value.discount) * 10) // 打折百分比
          }
        }
      };
      break;
      // 代金券
    case 'CASH':
      data = {
        card: {
          'card_type': 'CASH',
          'cash': {
            'base_info': base_info,
            'least_cost': parseFloat(value.least_cost).toFixed(2) * 100,
            'reduce_cost': parseFloat(value.reduce_cost).toFixed(2) * 100
          }
        }
      };
      break;
  }

  return data;
};

/**
 * 数据格式化处理
 * @param  {[type]} data [description]
 * @return {[type]}      [description]
 */
export const setPio = (value) => {
  // 格式化图片列表
  let photo_list = [];
  for (let i in value.photo_list) {
    let item = value.photo_list[i];
    photo_list[i] = {
      photo_url: item
    };
  }
  let business = {
    'business': {
      'base_info': {
        'sid': value.sid || value._id || '',
        'business_name': value.business_name,
        'branch_name': value.branch_name,
        'province': value.province,
        'city': value.city,
        'district': value.district,
        'address': value.address,
        'telephone': value.telephone,
        'categories': value.categories,
        'offset_type': 1,
        'longitude': value.longitude,
        'latitude': value.latitude,
        'photo_list': photo_list,
        'recommend': value.recommend,
        'special': value.special || '',
        'introduction': value.introduction || '',
        'open_time': value.open_time || '',
        'avg_price': Number(value.avg_price)
      }
    }
  };
  console.log('提交的', business);
  return business;
};
