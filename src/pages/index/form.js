/*
 * @Author: sean
 * @Date:   2020-06-02 19:56:57
 * @Last Modified by:   sean
 * @Last Modified time: 2020-06-04 16:39:53
 */
export default {
  'name': {
    'title': '菜单名称',
    'type': 'String',
    'required': true,
    form: {
      type: 'input',
      rules: {
        valid: {
          pattern: /^[a-zA-Z]\w+$/,
          message: '菜单名称必须为字母开头'
        }
      }
    }
  },
  'menuId': {
    'title': '菜单ID',
    'type': 'auto_increment',
    form: {
      hide: true,
      type: 'input'
    }
  },
  'parentId': {
    'title': '父级ID',
    'type': 'Number',
    'required': true,
    form: {
      type: 'tree',
      from: {
        'menus': [],
        'all': []
      },
      field: {
        'parentName': 'parentId',
        'titleName': 'title',
        'keyName': 'menuId'
      },
      value: {
        'parentName': -1,
        'titleName': '顶级菜单',
        'keyName': 0
      }
    }
  },
  'path': {
    'title': '菜单路径',
    'type': 'Array',
    'default': [],
    form: {
      hide: true,
      type: 'input'
    }
  },
  'component': {
    'title': '视图模板',
    'type': 'String',
    form: {
      hide: true,
      type: 'input'
    }
  },
  'icon': {
    'title': '菜单图标',
    'type': 'String',
    form: {
      type: 'input'
    }
  },
  'title': {
    'title': '菜单标题',
    'type': 'String',
    'required': true,
    form: {
      type: 'input'
    }
  },
  'show': {
    'title': '是否显示',
    'type': 'Boolean',
    default: true,
    form: {
      type: 'radio',
      value: [
        { title: '显示', key: true },
        { title: '隐藏', key: false }
      ]
    }
  },
  'hideChildren': {
    'title': '隐藏子集',
    'type': 'Boolean',
    'default': false,
    form: {
      type: 'radio',
      value: [
        { title: '显示', key: true },
        { title: '隐藏', key: false }
      ]
    }
  },
  'type': {
    'title': '菜单类型',
    'type': 'String',
    'required': true,
    default: 'PAGE',
    form: {
      type: 'radio',
      value: [
        { title: '目录', key: 'PATH' },
        { title: '页面', key: 'PAGE' }
      ]
    }
  },
  'sort': {
    'title': '菜单排序',
    'type': 'Number',
    'default': 200,
    form: {
      type: 'input'
    }
  },
  'rules': {
    'title': '规则列表',
    'type': 'Array',
    'default': [],
    form: {
      type: 'input'
    }
  }
};
